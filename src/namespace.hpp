#ifndef STREAM9_LINUX_EPOLL_TEST_SRC_NAMESPACE_HPP
#define STREAM9_LINUX_EPOLL_TEST_SRC_NAMESPACE_HPP

namespace stream9::json {}
namespace stream9::linux {}
namespace stream9::linux::epoll {}
namespace stream9::errors {}

namespace testing {

namespace lx { using namespace stream9::linux; }
namespace epo { using namespace stream9::linux::epoll; }
namespace json { using namespace stream9::json; }
namespace err { using namespace stream9::errors; }

} // namespace testing

#endif // STREAM9_LINUX_EPOLL_TEST_SRC_NAMESPACE_HPP
