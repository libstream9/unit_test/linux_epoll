#include <stream9/linux/epoll/epoll.hpp>
#include <stream9/linux/epoll/epoll.ipp>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>

namespace testing {

namespace lx = stream9::linux;
namespace json { using namespace stream9::json; }

using stream9::errors::print_error;

inline std::pair<lx::fd, lx::fd>
pipe()
{
    int fds[2] {};
    auto rc = ::pipe(fds);
    BOOST_REQUIRE(rc != -1);

    return { lx::fd(fds[0]), lx::fd(fds[1]) };
}

BOOST_AUTO_TEST_SUITE(epoll_)

    BOOST_AUTO_TEST_CASE(constructor_)
    {
        lx::epoll e;
    }

    BOOST_AUTO_TEST_CASE(add_1_)
    {
        try {
            lx::epoll e;

            auto [rfd, wfd] = pipe(); //TODO lx::pipe

            e.add(rfd, {
                .events = EPOLLIN,
                .data = { .fd = rfd }
            });
        }
        catch (...) {
            print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(add_2_)
    {
        try {
            lx::epoll e;

            auto [rfd, wfd] = pipe();

            e.add(rfd, EPOLLIN);
        }
        catch (...) {
            print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(modify_1_)
    {
        try {
            lx::epoll e;

            auto [rfd, wfd] = pipe(); //TODO lx::pipe

            e.add(rfd, {
                .events = EPOLLIN,
                .data = { .fd = rfd }
            });

            e.modify(rfd, {
                .events = EPOLLIN | EPOLLET,
                .data = { .fd = rfd }
            });
        }
        catch (...) {
            print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(modify_2_)
    {
        try {
            lx::epoll e;

            auto [rfd, wfd] = pipe();

            e.add(rfd, EPOLLIN);

            e.modify(rfd, EPOLLIN | EPOLLIN);
        }
        catch (...) {
            print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(remove_1_)
    {
        try {
            lx::epoll e;

            auto [rfd, wfd] = pipe(); //TODO lx::pipe

            e.add(rfd, EPOLLIN);

            e.remove(rfd);
        }
        catch (...) {
            print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(EPOLLIN_1_)
    {
        try {
            lx::epoll e;

            auto [rfd, wfd] = pipe();

            e.add(rfd, EPOLLIN);

            auto rc2 = ::write(wfd, "foo", 3);
            BOOST_REQUIRE(rc2 != -1);

            auto result = e.wait();

            auto expected = json::array {
                json::object {
                    { "events", "EPOLLIN" },
                    { "data", rfd },
                }
            };

            BOOST_TEST(json::value_from(result) == expected);
        }
        catch (...) {
            print_error();
            BOOST_CHECK(false);
        }
    }

    BOOST_AUTO_TEST_CASE(EPOLLOUT_1_)
    {
        try {
            lx::epoll e;

            auto [rfd, wfd] = pipe();

            e.add(wfd, EPOLLOUT);

            auto result = e.wait();

            auto expected = json::array {
                json::object {
                    { "events", "EPOLLOUT" },
                    { "data", wfd },
                }
            };

            BOOST_TEST(json::value_from(result) == expected);
        }
        catch (...) {
            print_error();
            BOOST_CHECK(false);
        }
    }

    BOOST_AUTO_TEST_CASE(EPOLLERR_1_)
    {
        try {
            lx::epoll e;

            auto [rfd, wfd] = pipe();

            e.add(wfd, EPOLLERR);

            // closing reading side causes an error on writing side
            rfd.close();

            auto result = e.wait();

            auto expected = json::array {
                json::object {
                    { "events", "EPOLLERR" },
                    { "data", wfd },
                }
            };

            BOOST_TEST(json::value_from(result) == expected);
        }
        catch (...) {
            print_error();
            BOOST_CHECK(false);
        }
    }

    BOOST_AUTO_TEST_CASE(EPOLLHUP_1_)
    {
        try {
            lx::epoll e;

            auto [rfd, wfd] = pipe();

            e.add(rfd, EPOLLIN);

            // closing writing side causes an hung up on reading side
            wfd.close();

            auto result = e.wait();

            auto expected = json::array {
                json::object {
                    { "events", "EPOLLHUP" },
                    { "data", rfd },
                }
            };

            BOOST_TEST(json::value_from(result) == expected);
        }
        catch (...) {
            print_error();
            BOOST_CHECK(false);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // epoll_

} // namespace testing
